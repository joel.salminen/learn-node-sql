const mysql = require('mysql');
const express = require('express');

const app = express();

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'games',
  port: 3306
});

connection.connect((err) => {
  if (err) {
    console.error(`error connecting: ${err.stack}`);
    return;
  }

  console.log(`connected as id ${connection.threadId}`);
  app.listen(3000, () => {
    console.info('Listening on port 3000');
  });
});

app.use('/', (req, res) => {
  connection.query('SELECT * FROM games', (error, results, fields) => {
    if (error) {
      throw error;
    }
    console.log(results);
    res.send(results);
  });
});

// connection.end();
