module.exports = {
  extends: 'airbnb-base',
  rules: {
    'comma-dangle': 0,
    'no-unused-vars': 'warn',
    'no-underscore-dangle': 'off',
    'func-names': 'off',
  },
  env: { node: true, jest: true },
};
